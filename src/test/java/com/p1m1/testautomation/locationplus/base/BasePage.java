package com.p1m1.testautomation.locationplus.base;


import static com.p1m1.testautomation.locationplus.tuborg.constants.ConstantsMainPage.*;

import java.util.Date;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;

import com.google.common.base.Function;

public abstract class BasePage extends BasePageUtil {

	public BasePage(WebDriver driver) {
		super(driver);
	}

	/**
	 * sub-nav menü deki kategori alanlarına mainIndex ve subIndexes verilerek
	 * tıklanılmasını sağlar.
	 * 
	 * @throws InterruptedException
	 */
	@Override
	protected void clickSubMenus(int mainIndex, int... subIndexes) throws InterruptedException {

	}

	/**
	 * game-nav alanında ki bülten, kupondaş v.s menülerine mainIndex verilerek
	 * tıklanılması sağlanır.
	 */
	@Override
	protected void clickGameMenus(int mainIndex) {
	}

	/**
	 * Get attr data-value
	 * 
	 * @param by
	 * @param index
	 * @return
	 */
	protected String getAttributeDataValue(By by, int... index) {
		return findElement(by, index).getAttribute("data-value");
	}

	/**
	 * @param name
	 * @param value
	 * @param domain
	 * @param path
	 * @param expiry
	 */
	protected void addCookie(String name, String value, String domain, String path, Date expiry) {
		driver.manage().addCookie(new Cookie(name, value, domain, path, expiry));
	}

	@Override
	protected void untilAjaxComplete() {
		new FluentWait<WebDriver>(driver).withTimeout(15, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class, StaleElementReferenceException.class)
				.until(new Function<WebDriver, Boolean>() {
					@Override
					public Boolean apply(WebDriver input) {
						return untilElementDisappearB(PACE_RUNNIG);
					}
				});
	}
	
	/**
	 * 
	 */
	protected void untilLoadinBarDissapear() {
		new FluentWait<WebDriver>(driver).withTimeout(15, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS)
		.ignoring(NoSuchElementException.class, StaleElementReferenceException.class)
		.until((Function<WebDriver, Boolean>) input -> untilElementDisappearB(LOADING_BAR));
	}

	/**
	 * Placeholder alanının kontrolü sağlanır.
	 * 
	 * @param cssSelector
	 * @param value
	 * @param index
	 * @return
	 */
	protected boolean checkPlaceHolder(By by, String value, int... index) {
		return getAttribute(by, "placeholder", index).equals(value);
	}

}
