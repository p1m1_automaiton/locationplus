package com.p1m1.testautomation.locationplus.base;

import static org.testng.Assert.*;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.RuleBasedCollator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.p1m1.testautomation.locationplus.util.Configuration;

/**
 * @author emre.orhan
 * @version 3.0
 * @since 2016-12-16
 */
public abstract class BasePageUtil {

	protected Configuration config;
	protected WebDriver driver;
	protected final Logger log = Logger.getLogger(getClass());
	protected WebDriverWait wait;

	public BasePageUtil(WebDriver driver) {
		this.config = Configuration.getInstance();
		this.driver = driver;
		this.wait = new WebDriverWait(driver, config.getWaitTime());
	}

	protected JavascriptExecutor getJSExecutor() {
		return (JavascriptExecutor) driver;
	}

	/**
	 * Wait anyone until ajax complete
	 */
	protected abstract void untilAjaxComplete();

	/**
	 * @param mainIndex
	 * @param subIndexes
	 * @throws InterruptedException
	 */
	protected abstract void clickSubMenus(int mainIndex, int... subIndexes) throws InterruptedException;

	/**
	 * @param mainIndex
	 */
	protected abstract void clickGameMenus(int mainIndex);

	/**
	 * Javascript executer
	 * 
	 * @param jsStmt
	 * @param wait
	 * @return
	 */
	protected Object executeJS(String jsStmt, boolean wait) {
		return wait ? getJSExecutor().executeScript(jsStmt, "") : getJSExecutor().executeAsyncScript(jsStmt, "");
	}

	/**
	 * Javascript executer
	 * 
	 * @param script
	 * @param obj
	 */
	protected void executeJS(String script, Object... obj) {
		getJSExecutor().executeScript(script, obj);
	}

	/**
	 * Javascript executer boolean
	 * 
	 * @param jsStmt
	 * @return
	 */
	protected boolean executeBoolJS(String jsStmt) {
		return "true".equals(executeJS(jsStmt, true).toString());
	}

	/**
	 * Element null exception
	 * 
	 * @param by
	 * @param index
	 */
	protected void nullElementException(By by, int... index) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("ELEMENT (");
		stringBuilder.append(by);
		stringBuilder.append(",");
		stringBuilder.append(index.length > 0 ? index[0] : "");
		stringBuilder.append(") NOT EXISTS; AUTOMATION DATAS MAY BE INVALID!");
		throw new NullPointerException(stringBuilder.toString());
	}

	/**
	 * find WebElement
	 * 
	 * @param by
	 * @param index
	 * @return
	 */
	protected WebElement findElement(By by, int... index) {
		if (index.length == 0) {
			// highlightElement(element)
			return driver.findElement(by);
		} else if (index[0] >= 0) {
			List<WebElement> elements = driver.findElements(by);
			if (!elements.isEmpty() && index[0] <= elements.size()) {
				// highlightElement(elements.get(index[0]))
				return elements.get(index[0]);
			}
		}
		return null;
	}

	/**
	 * Highlight Web Element
	 * 
	 * @param element
	 */
	protected void highlightElement(WebElement element) {
		executeJS("arguments[0].setAttribute('style', arguments[1]);", element, "color: red;");
		// border: 1px dashed red; border
	}

	/**
	 * find WebElement parent
	 * 
	 * @param parent
	 * @param by
	 * @param index
	 * @return
	 */
	protected WebElement findElement(WebElement parent, By by, int... index) {
		if (index.length == 0) {
			return parent.findElement(by);
		} else if (index[0] >= 0) {
			List<WebElement> elements = parent.findElements(by);
			if (!elements.isEmpty() && index[0] <= elements.size()) {
				return elements.get(index[0]);
			}
		}
		return null;
	}

	/**
	 * findElement parent by, by
	 * 
	 * @param parent
	 * @param by
	 * @param index
	 * @return
	 */
	protected WebElement findElement(By parent, By by, int... index) {
		return findElement(findElement(parent), by, index);
	}

	/**
	 * find WebElement List
	 * 
	 * @param by
	 * @return List
	 */
	protected List<WebElement> findElements(By by) {
		return driver.findElements(by);
	}

	/**
	 * wait for WebElement clickable
	 * 
	 * @param by
	 * @param index
	 */
	protected void untilElementClickable(By by, int... index) {
		untilElementClickable(findElement(by, index));
	}

	/**
	 * wait for WebElement clickable
	 * 
	 * @param element
	 */
	protected void untilElementClickable(WebElement element) {
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	/**
	 * scrollTo WebElement location
	 * 
	 * @param x
	 * @param y
	 */
	protected void scrollTo(int x, int y) {
		String jsStmt = String.format("window.scrollTo(%d, %d);", x, y);
		executeJS(jsStmt, true);
	}

	/**
	 * scrollTo WebElement scope location
	 * 
	 * @param element
	 */
	protected void scrollToElement(WebElement element) {
		if (element != null) {
			scrollTo(element.getLocation().getX(), element.getLocation().getY());
		}
	}

	/**
	 * scrollTo WebElement
	 * 
	 * @param by
	 * @param index
	 */
	protected void scrollToElementC(By by, int... index) {
		Coordinates coordinate = ((Locatable) findElement(by, index)).getCoordinates();
		coordinate.onPage();
		coordinate.inViewPort();
	}

	/**
	 * scrollTo WebElement scope location +x +y
	 * 
	 * @param by
	 * @param index
	 * @param x
	 * @param y
	 */
	protected void scrollToElement(By by, int x, int y) {
		WebElement element = findElement(by);
		if (element != null) {
			scrollTo(element.getLocation().getX() + x, element.getLocation().getY() + y);
		}
	}

	/**
	 * scrollTo find WebElement scope location
	 * 
	 * @param by
	 * @param index
	 */
	protected void scrollToElement(By by, int... index) {
		scrollToElement(findElement(by, index));
	}

	/**
	 * scrollTo Page End
	 */
	protected void scrollToPageEnd() {
		executeJS("window.scrollTo(0, document.body.scrollHeight)", true);
	}

	/**
	 * scrollTo Page Up
	 */
	protected void scrollToPageUp() {
		executeJS("window.scrollTo(document.body.scrollHeight, 0)", true);
	}

	/**
	 * wait for web element present -Preferred
	 * 
	 * @param by
	 * @param index
	 * @param by
	 */
	protected void waitForElementPresent(By by, int... index) {
		wait.ignoring(StaleElementReferenceException.class);
		wait.until((ExpectedCondition<Boolean>) (WebDriver webDriver) -> {
			return findElement(by, index) != null && findElement(by, index).isDisplayed();
		});
	}

	/**
	 * wait for web element present -Preferred Boolean
	 * 
	 * @param by
	 * @param index
	 * @return
	 */
	protected boolean waitForElementPresentB(final By by, final int... index) {
		wait.ignoring(StaleElementReferenceException.class);
		return wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver webDriver) {
				WebElement element = findElement(by, index);
				return element != null && element.isDisplayed();
			}
		});
	}

	/**
	 * wait for web element present
	 * 
	 * @param element
	 */
	protected void waitForElementPresent(final WebElement element) {
		wait.ignoring(StaleElementReferenceException.class);
		wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver webDriver) {
				return element != null && element.isDisplayed();
			}
		});
	}

	/**
	 * wait for web element present
	 * 
	 * @param driver
	 * @param by
	 * @param timeOutInSeconds
	 * @return
	 */
	protected WebElement waitForElementPresent(By by, int timeOutInSeconds) {
		WebElement element;
		try {
			WebDriverWait waitSeconds = (WebDriverWait) new WebDriverWait(driver, timeOutInSeconds)
					.ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
			element = waitSeconds.until(ExpectedConditions.presenceOfElementLocated(by));
			return element;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return null;
	}

	/**
	 * wait for web element dissapear
	 * 
	 * @param by
	 * @param timeOutInSeconds
	 */
	protected void waitForElementDissapear(By by, int timeOutInSeconds) {
		try {
			WebDriverWait waitSeconds = (WebDriverWait) new WebDriverWait(driver, timeOutInSeconds)
					.ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class);
			waitSeconds.until(ExpectedConditions.invisibilityOfElementLocated(by));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * Expects the element disappears
	 * 
	 * @param by
	 */
	protected void untilElementDisappear(By by) {
		wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
	}

	/**
	 * Expects the element disappears return boolean
	 * 
	 * @param by
	 */
	protected boolean untilElementDisappearB(By by) {
		return wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
	}

	/**
	 * Expects the element appears
	 * 
	 * @param by
	 */
	protected void untilElementAppear(By by) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	/**
	 * Expects the element appears
	 * 
	 * @param by
	 * @param index
	 */
	protected void untilElementAppear(By by, int... index) {
		wait.until(ExpectedConditions.visibilityOf(findElement(by, index)));
	}

	/**
	 * Expects the element presence
	 * 
	 * @param by
	 */
	protected void untilElementPresence(By by) {
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
	}

	/**
	 * Switch to frame
	 * 
	 * @param by
	 */
	protected void switchToFrame(By by, int... index) {
		driver.switchTo().defaultContent();
		untilElementAppear(by, index);
		driver.switchTo().frame(findElement(by, index));
	}

	/**
	 * Alert popup handle
	 * 
	 * @param acceptAndDismiss
	 * @throws InterruptedException
	 */
	protected void alertPopup(boolean acceptAndDismiss) {
		wait.until(ExpectedConditions.alertIsPresent());
		Alert alert = driver.switchTo().alert();
		if (acceptAndDismiss) {
			alert.accept();
		} else {
			alert.dismiss();
		}
	}

	/**
	 * Mouse Hover and Click
	 * 
	 * @param by
	 * @param click
	 * @param index
	 */
	protected void hoverElement(By by, boolean click, int... index) {
		hoverElement(findElement(by, index));
		untilElementPresence(by);
		if (click) {
			clickElement(by, index);
		}
	}

	/**
	 * Mouse Hover and Click
	 * 
	 * @param by
	 * @param index
	 */
	protected void hoverElementAndClick(By by, int... index) {
		hoverElement(by, true, index);
	}

	/**
	 * Mouse Hover
	 * 
	 * @param by
	 * @param index
	 */
	protected void hoverElement(By by, int... index) {
		hoverElement(by, false, index);
	}

	/**
	 * Mouse Hover
	 * 
	 * @param by
	 * @param index
	 */
	protected void hoverElementJS(By by, int... index) {
		hoverElementJavasript(findElement(by, index));
	}

	/**
	 * Mouse Hover
	 * 
	 * @param element
	 */
	protected void hoverElement(WebElement element) {
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
	}

	/**
	 * Mouse Hover
	 * 
	 * @param element
	 */
	protected void hoverElementJavasript(WebElement element) {
		String strJavaScript = "var element = arguments[0];"
				+ "var mouseEventObj = document.createEvent('MouseEvents');"
				+ "mouseEventObj.initEvent( 'mouseover', true, true );" + "element.dispatchEvent(mouseEventObj);";
		executeJS(strJavaScript, element);
	}

	/**
	 * Mouse Hover List
	 * 
	 * @param cssSelector
	 * @param click
	 *            ? true(click) : only hover
	 */
	protected void hoverNClickAllElement(By by, boolean... click) {
		for (WebElement webElement : findElements(by)) {
			hoverElement(webElement);
			if (click != null && click[0]) {
				webElement.click();
			}
		}
	}

	/**
	 * Drag and Drop Element
	 * 
	 * @param by
	 * @param xOffset
	 * @param yOffset
	 * @throws InterruptedException
	 */
	protected void dragElement(By by, int xOffset, int yOffset) throws InterruptedException {
		Actions a = new Actions(driver);
		a.dragAndDropBy(findElement(by), xOffset, yOffset).build().perform();
		timeUnitSeconds(2);
	}

	/**
	 * Find Select Menu
	 * 
	 * @param by
	 * @return
	 */
	protected Select selectOption(By by) {
		untilElementAppear(by);
		return new Select(findElement(by));
	}

	/**
	 * Select menu with text click.
	 * 
	 * @param by
	 *            = id olması tercih edilir.
	 * @param value
	 *            = Görünen text'i
	 */
	protected void selectOptionClick(By by, String value) {
		selectOption(by).selectByVisibleText(value);
	}

	/**
	 * Select menu list get.
	 * 
	 * @param by
	 * @param value
	 * @return
	 */
	protected List<WebElement> selectOptionList(By by) {
		return selectOption(by).getOptions();
	}

	/**
	 * Select menu first select get
	 * 
	 * @param by
	 * @param value
	 * @return
	 */
	protected WebElement selectOptionFirstSelect(By by) {
		return selectOption(by).getFirstSelectedOption();
	}

	/**
	 * Select menu get selected text
	 * 
	 * @param by
	 * @param value
	 * @return
	 */
	protected String selectOptionSelectedText(By by) {
		return selectOption(by).getFirstSelectedOption().getText();
	}

	/**
	 * Assertion Control(True)
	 * 
	 * @param message
	 * @param condition
	 */
	protected void assertionTrue(String message, boolean condition) {
		assertTrue(condition, message);
	}

	/**
	 * Assertion Control(True)
	 * 
	 * @param condition
	 */
	protected void assertionTrue(boolean condition) {
		assertTrue(condition);
	}

	/**
	 * Assertion Control(False)
	 * 
	 * @param message
	 * @param condition
	 */
	protected void assertionFalse(String message, boolean condition) {
		assertFalse(condition, message);
	}

	/**
	 * Assertion Control(False)
	 * 
	 * @param condition
	 */
	protected void assertionFalse(boolean condition) {
		assertFalse(condition);
	}

	/**
	 * Assertion Control(Equals)
	 * 
	 * @param message
	 * @param expected
	 * @param actual
	 */
	protected void assertionEquals(String message, Object expected, Object actual) {
		assertEquals(expected, actual, message);
	}

	/**
	 * Assertion Fail
	 * 
	 * @param message
	 */
	protected void assertFail(String message) {
		fail(message);
	}

	/**
	 * Click web element(String id)
	 * 
	 * @param id
	 */
	protected void clickElementById(String id) {
		clickElementById(id, true);
	}

	/**
	 * Click web element(Strin id) waitForAjax
	 * 
	 * @param id
	 * @param waitForAjax
	 */
	protected void clickElementById(String id, boolean waitForAjax) {
		clickElement(By.id(id), waitForAjax);
	}

	/**
	 * Click web element (By by, int index(getIndex))
	 * 
	 * @param by
	 * @param index
	 */
	protected void clickElement(By by, int... index) {
		clickElement(by, false, index);
	}

	/**
	 * Click web element (By by, int index(getIndex)) waitforAjax
	 * 
	 * @param by
	 * @param waitForAjax
	 * @param index
	 */
	protected void clickElement(By by, boolean waitForAjax, int... index) {
		WebElement element = null;
		try {
			element = findElement(by, index);
		} catch (Exception e) {
			log.error("ERROR :", e);
			assertFail("Element Not Found :" + e.getMessage());
		}
		if (element == null) {
			nullElementException(by, index);
		} else {
			if (!isElementDisplayed(by, index)) {
				scrollTo(element.getLocation().getX(), element.getLocation().getY());
			}
			waitForElementPresent(element);
			untilElementClickable(element);
			log.info("ELEMENT Clicked (" + element + ")");
			element.click();
			if (waitForAjax) {
				untilAjaxComplete();
			}
		}
	}

	/**
	 * Click web element parent (By parent, By by, int index(getIndex))
	 * waitforAjax
	 * 
	 * @param parent
	 * @param by
	 * @param waitForAjax
	 * @param index
	 */
	protected void clickElement(By parent, By by, boolean waitForAjax, int... index) {
		WebElement element = findElement(findElement(parent, index[0]), by, index[1]);
		if (element == null) {
			nullElementException(by, index);
		} else {
			if (!element.isDisplayed()) {
				scrollTo(element.getLocation().getX(), element.getLocation().getY());
			}
			untilElementClickable(element);
			waitForElementPresent(element);
			element.click();
			if (waitForAjax) {
				untilAjaxComplete();
			}
		}
	}

	protected void clickElement(WebElement element, boolean waitForAjax) {
		WebElement foundedElement = element;
		if (foundedElement == null) {
			// nullElementException(by, index)
		} else {
			if (!foundedElement.isDisplayed()) {
				scrollTo(foundedElement.getLocation().getX(), foundedElement.getLocation().getY());
			}
			untilElementClickable(foundedElement);
			waitForElementPresent(foundedElement);
			foundedElement.click();
			if (waitForAjax) {
				untilAjaxComplete();
			}
		}
	}

	/**
	 * Click web element parent (By parent, By by, int index(getIndex))
	 * waitforAjax=false
	 * 
	 * @param parent
	 * @param by
	 * @param index
	 */
	protected void clickElement(By parent, By by, int... index) {
		clickElement(parent, by, false, index[0], index[1]);
	}

	/**
	 * Click web element with JavaScript
	 * 
	 * @param cssSelector
	 * @param index
	 */
	protected void clickElementJS(String cssSelector, int... index) {
		if (!isElementExists(cssSelector))
			return;
		String jsStmt = String.format("$('%s').click()['%s'];", cssSelector, index);
		executeJS(jsStmt, true);
	}

	/**
	 * Click web element with JavaScript
	 * 
	 * @param by
	 * @param index
	 */
	protected void clickElementJS(By by, int... index) {
		getJSExecutor().executeScript("arguments[0].click();", findElement(by, index));
	}

	/**
	 * Provide data input(By by, String text, int index)
	 * 
	 * @param by
	 * @param text
	 * @param index
	 */
	protected void fillInputField(By by, String text, int... index) {
		fillInputField(by, text, false, index);
	}

	/**
	 * Provide data input(By by, String text, int index) press enter
	 * 
	 * @param by
	 * @param text
	 * @param pressEnter
	 * @param index
	 */
	protected void fillInputField(By by, String text, boolean pressEnter, int... index) {
		WebElement element = null;
		try {
			element = findElement(by, index);
		} catch (Exception e) {
			log.error("ERROR :", e);
			assertFail("Element Not Found :" + e.getMessage());
		}
		if (element == null) {
			nullElementException(by, index);
		} else if (element.isEnabled()) {
			untilElementClickable(element);
			log.info("ELEMENT Send Keys : " + text + " (" + element + ")");
			element.clear();
			element.sendKeys(text);
			if (pressEnter) {
				element.sendKeys(Keys.ENTER);
			}
		}
	}

	/**
	 * Checking the presence of qualification
	 * 
	 * @param cssSelector
	 * @param index
	 * @return
	 */
	protected boolean isElementExists(String cssSelector, int... index) {
		String jsStmt = index.length == 0 || index[0] < 0 ? String.format("return $('%s').size()>0", cssSelector)
				: String.format("return $('%s').size()>0 && $('%s').eq(%d).size()>0", cssSelector, cssSelector,
						index[0]);
		Object result = executeJS(jsStmt, true);
		return result != null && "true".equalsIgnoreCase(result.toString());
	}

	/**
	 * Checking the presence of qualification
	 * 
	 * @param by
	 * @return
	 */
	protected boolean isElementExists(By by, int... index) {
		if (findElement(by, index).isDisplayed()) {
			return true;
		}
		return false;
	}

	/**
	 * Checking the presence of qualification
	 * 
	 * @param by
	 * @return
	 */
	protected boolean isElementPresent(By by) {
		if (!findElements(by).isEmpty()) {
			return true;
		}
		return false;
	}

	/**
	 * Qualification to control the visibility
	 * 
	 * @param by
	 * @param index
	 * @return
	 */
	protected boolean isElementInView(By by, int... index) {
		String jsStmt;
		if (index.length == 0 || index[0] < 0) {
			jsStmt = String.format("return arguments[%s].size()>0;", by);
		} else {
			jsStmt = String.format("return arguments[%s%s].size()>0;", by, index[0]);
		}
		Object result = executeJS(jsStmt, true);
		return result != null && "true".equalsIgnoreCase(result.toString());
	}

	/**
	 * Qualification to control the visibility
	 * 
	 * @param by
	 * @param index
	 * @return
	 */
	protected boolean isElementDisplayed(By by, int... index) {
		try {
			return findElement(by, index).isDisplayed();
		} catch (NoSuchElementException e) {
			log.debug("Element Is Not Displayed", e);
			return false;
		}
	}

	/**
	 * Qualification to control the visibility
	 * 
	 * @param element
	 * @return
	 */
	protected boolean isElementDisplayed(WebElement element) throws InterruptedException {
		if (element.isDisplayed()) {
			return true;
		}
		return false;
	}

	/**
	 * Qualification to control the enabled
	 * 
	 * @param by
	 * @param index
	 * @return
	 */
	protected boolean isElementEnabled(By by, int... index) {
		try {
			return findElement(by, index).isEnabled();
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * The expected qualification visibility
	 * 
	 * @param by
	 * @param waitTime
	 * @param index
	 * @return
	 */
	protected boolean isElementDisplayedWait(By by, int waitTime) {
		try {
			return waitForElementPresent(by, waitTime).isDisplayed();
		} catch (Exception e) {
			log.debug(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * Navigate to url
	 * 
	 * @param url
	 */
	protected void navigateTo(String url) {
		driver.navigate().to(url);
	}

	/**
	 * Refresh page
	 */
	protected void refreshTo() {
		driver.navigate().refresh();
	}

	/**
	 * Get Element Attribute
	 * 
	 * @param by
	 * @param attr
	 * @param index
	 * @return
	 */
	protected String getAttribute(By by, String attr, int... index) {
		return findElement(by, index).getAttribute(attr);
	}

	/**
	 * Get Element Attribute for Value
	 * 
	 * @param by
	 * @param index
	 * @return
	 */
	protected String getAttributeValue(By by, int... index) {
		return findElement(by, index).getAttribute("value");
	}

	/**
	 * Get page url
	 * 
	 * @return
	 */
	protected String getCurrentUrl() {
		return driver.getCurrentUrl().trim();
	}

	/**
	 * Get page source
	 * 
	 * @return
	 */
	protected String getPageSource() {
		return driver.getPageSource();
	}

	/**
	 * Get page title
	 * 
	 * @return
	 */
	protected String getTitle() {
		return driver.getTitle();
	}

	/**
	 * Get element text
	 * 
	 * @param by
	 * @param index
	 * @return
	 */
	protected String getText(By by, int... index) {
		return findElement(by, index).getText();
	}

	/**
	 * Get element list size
	 * 
	 * @param by
	 * @return
	 */
	protected Integer getSize(By by) {
		return findElements(by).size();
	}

	/**
	 * Get Lenght
	 * 
	 * @param value
	 * @return
	 */
	protected Integer getLenght(String value) {
		return value.length();
	}

	/**
	 * Go forward backs
	 */
	public void goBack() {
		driver.navigate().back();
	}

	/**
	 * Call the page
	 * 
	 * @param page
	 */
	protected void callPage(String page) {
		driver.get(getCurrentUrl() + page);
	}

	/**
	 * Turkish character to convert
	 * 
	 * @param string
	 * @return
	 */
	protected static String convertTurkishChar(String string) {
		String returnString = string;
		char[] turkishChars = new char[] { 0x131, 0x130, 0xFC, 0xDC, 0xF6, 0xD6, 0x15F, 0x15E, 0xE7, 0xC7, 0x11F,
				0x11E };
		char[] englishChars = new char[] { 'i', 'I', 'u', 'U', 'o', 'O', 's', 'S', 'c', 'C', 'g', 'G' };
		for (int i = 0; i < turkishChars.length; i++) {
			returnString = returnString.replaceAll(new String(new char[] { turkishChars[i] }),
					new String(new char[] { englishChars[i] }));
		}
		return returnString;
	}

	/**
	 * Balance for Integer
	 * 
	 * @param string
	 * @return
	 */
	protected int getCurrencyAsInt(By balance) {
		String getTimes = getText(balance);
		getTimes = getTimes.replaceAll("\\.", "");
		getTimes = getTimes.replaceAll("TL", "");
		getTimes = getTimes.replaceAll(" ", "");
		return new Integer(getTimes.split(",")[0]);
	}

	/**
	 * Text Contains Control
	 * 
	 * @param by
	 * @param text
	 *            contains
	 * @return
	 */
	protected boolean isTextContains(By by, String text, int... index) {
		try {
			return getText(by, index).contains(text);
		} catch (NullPointerException e) {
			log.error(e.getMessage(), e);
			return false;
		} finally {
			log.info("Found Text :" + getText(by, index));
			log.info("Searching Text :" + text);
		}
	}

	/**
	 * Text Equals Control
	 * 
	 * @param by
	 * @param text
	 *            equals
	 * @return
	 */
	protected boolean isTextEquals(By by, String text, int... index) {
		try {
			return getText(by, index).equals(text);
		} catch (NullPointerException e) {
			log.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * Text Present Page Source
	 * 
	 * @param text
	 * @return
	 */
	protected boolean isTextPresent(String text) {
		try {
			return getPageSource().contains(text);
		} catch (NullPointerException e) {
			log.error("Does Not Exists Element", e);
			return false;
		}
	}

	/**
	 * JavaScript Clicker
	 * 
	 * @param driver
	 * @param element
	 */
	protected void javaScriptClicker(WebDriver driver, WebElement element) {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("var evt = document.createEvent('MouseEvents');"
				+ "evt.initMouseEvent('click',true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0,null);"
				+ "arguments[0].dispatchEvent(evt);", element);
	}

	/**
	 * Turkish Character Ranking
	 *
	 * @return
	 */
	protected RuleBasedCollator getTurkishCollator() {
		String turkish = " < '.' < 0 < 1 < 2 < 3 < 4 < 5 < 6 < 7 < 8 < 9 < a, A < b, B < c, C < ç, Ç < d, D < e, E < f, F"
				+ "< g, G < ğ, Ğ < h, H < ı, I < i, İ < j, J < k, K < l, L "
				+ "< m, M < n, N < o, O < ö, Ö < p, P < q, Q < r, R < s, S"
				+ "< ş, Ş < t, T < u, U < ü, Ü < v, V < w, W < x, X < y, Y < z, Z";
		try {
			return new RuleBasedCollator(turkish);
		} catch (ParseException e) {
			log.error("Does Not Exists Element", e);
		}
		return null;
	}

	/**
	 * Create md5
	 * 
	 * @param password
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws Exception
	 */
	public static String md5(String password) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(password.getBytes());
		byte[] byteData = md.digest();
		StringBuilder sb = new StringBuilder();
		for (byte b : byteData) {
			sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}

	/**
	 * Delete cookie
	 * 
	 * @param cookieName
	 * @return
	 */
	public BasePageUtil deleteCookie(String cookieName) {
		driver.manage().deleteCookieNamed(cookieName);
		return this;
	}

	/**
	 * Move To Element
	 * 
	 * @param element
	 */
	protected void moveToElement(WebElement element) {
		Actions actions = new Actions(driver);
		actions.moveToElement(element);
		actions.perform();
	}

	/**
	 * Desired length random string
	 * 
	 * @param size
	 * @return
	 */
	protected String createRandomString(int size) {
		return RandomStringUtils.randomAlphanumeric(size);
	}

	/**
	 * The desirable range random int
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	protected int createRandomInteger(int start, int end) {
		int randomNumber;
		if (start > end) {
			throw new IllegalArgumentException("Start cannot exceed End.");
		} else {
			Random random = new Random();
			randomNumber = random.nextInt((end - start) + 1) + start;
		}
		return randomNumber;
	}

	/**
	 * The remove in text of number
	 * 
	 * @param regexText
	 * @return
	 */
	protected String regexString(String regexText) {
		return regexText.replaceAll("\\d", "");
	}

	/**
	 * Convert to month
	 * 
	 * @param month
	 * @return
	 */
	protected String convertToMonth(String month) {
		String[] monthList = { "Month", "Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos",
				"Eylül", "Ekim", "Kasım", "Aralık" };
		if (month.length() > 2) {
			for (String selectedMonth : monthList) {
				if (selectedMonth.equals(month)) {
					return month;
				}
			}
		} else if (month.length() == 1 || month.length() == 2) {
			return monthList[Integer.parseInt(month)];
		}
		return "Does Not Exists Data Date";
	}

	/**
	 * Generate css Selector By
	 * 
	 * @param cssSelector
	 * @return
	 */
	protected By generateCss(String cssSelector) {
		return By.cssSelector(cssSelector);
	}

	/**
	 * Generate xpath By
	 * 
	 * @param xPath
	 * @return
	 */
	protected By generateXpath(String xPath) {
		return By.xpath(xPath);
	}

	/**
	 * Sleep Minute
	 * 
	 * @param sleepTime
	 * @throws InterruptedException
	 */
	protected void timeUnitMinutes(int sleepTime) throws InterruptedException {
		TimeUnit.MINUTES.sleep(sleepTime);
	}

	/**
	 * Sleep Seconds
	 * 
	 * @param sleepTime
	 * @throws InterruptedException
	 */
	protected void timeUnitSeconds(int sleepTime) throws InterruptedException {
		TimeUnit.SECONDS.sleep(sleepTime);
	}

	/**
	 * Sleep Mili Seconds
	 * 
	 * @param sleepTime
	 * @throws InterruptedException
	 */
	protected void timeUnitMiliSeconds(int sleepTime) throws InterruptedException {
		TimeUnit.MILLISECONDS.sleep(sleepTime);
	}
}
