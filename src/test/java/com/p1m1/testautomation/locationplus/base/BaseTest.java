package com.p1m1.testautomation.locationplus.base;

import java.lang.reflect.Method;
import java.net.MalformedURLException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import com.p1m1.testautomation.locationplus.util.Browser;
import com.p1m1.testautomation.locationplus.util.Configuration;
import com.p1m1.testautomation.locationplus.util.TestListener;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

@Listeners(TestListener.class)
public class BaseTest {

	private static WebDriver driver;
	protected ExtentReports extentReports = null;
	protected ExtentTest extentTest = null;
	private String methodName = null;
	protected static final Logger log = Logger.getLogger(BaseTest.class);
	protected static Configuration config = Configuration.getInstance();
	protected static Browser browser = new Browser();

	// "kiosk"
	@BeforeMethod
	public void setUp(Method method) throws MalformedURLException {
		PropertyConfigurator.configure("properties/log4j.properties");
		//methodName = method.getName()
		//log.info("TEST NAME :" + getMethodName())
		log.info("Settings Intstallion Start");
		browser.setBrowser(config.getBrowserName(), config.getBrowserVersion(), config.getServerUrl(),
				config.getImplicitlyWait());
	}

	@AfterMethod
	public void tearDown() throws InterruptedException {
		getDriver().quit();
	}

	public static WebDriver getDriver() {
		return driver;
	}

	public static void setDriver(RemoteWebDriver driver) {
		BaseTest.driver = driver;
	}

	public String getExcelName() {
		return config.getExcelName();
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
}
