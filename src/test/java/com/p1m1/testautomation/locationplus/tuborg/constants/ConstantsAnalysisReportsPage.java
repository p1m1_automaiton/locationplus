package com.p1m1.testautomation.locationplus.tuborg.constants;

import org.openqa.selenium.By;

public class ConstantsAnalysisReportsPage extends ConstantsMainPage{

	public static final By SELECT_ANALYSIS_REPORT = By.id("selectAnalysisReport");
	public static final By LOAD_ANALYSIS_BTN = By.id("btnLoadAnalysis");
	public static final By DRAW_GRAPHIC_BTN = By.id("btnDrawGraphic");
	public static final By GRAPHICS = By.className("highcharts-series");
}
