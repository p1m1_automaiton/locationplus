package com.p1m1.testautomation.locationplus.tuborg.constants;

import org.openqa.selenium.By;

/**
 * @author emreorhan
 *
 */
public class ConstantsDataAnalysisPage extends ConstantsMainPage {

	/**
	 * Data Analyze Constants
	 */
	public static final By ADD_LOCATION_BTN = By.cssSelector("input[ng-click='addLocation()']");
	public static final By REFERENCEDATA_SELECTBOX = By.name("referenceData");
	public static final By QUERY_COLUMN_SELECTBOX = By.name("queryColumn");
	public static final By CRITERIA_COLUMN_SELECTBOX = By.id("selectCriteria");
	public static final By VALUE_COLUMN_SELECTBOX = By.id("selectValue");
	public static final By AND_BTN = By.id("btnAnd");
	public static final By OR_BTN = By.id("btnOr");
	public static final By AND_OR_TXT = By.id("txtAndOrText");
	/**
	 * Data Analyze Constants
	 */

	/**
	 * FILTER CONSTANTS
	 */
	public static final By VALUE_TEXT_BOX = By.id("valueTextbox");
	public static final By ADD_QUERY_BTN = By.cssSelector("input[ng-click='addQueryClicked()']");
	public static final By SEARCH_QUERY_BTN = By.cssSelector("input[ng-click='queryClicked()']");
	/**
	 * FILTER CONSTANTS
	 */
	
	public static final By MAP_MARKER = By.cssSelector(".leaflet-marker-icon.leaflet-clickable");
}
