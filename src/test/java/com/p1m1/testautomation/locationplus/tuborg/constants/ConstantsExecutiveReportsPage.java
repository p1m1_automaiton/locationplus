package com.p1m1.testautomation.locationplus.tuborg.constants;

import org.openqa.selenium.By;

public class ConstantsExecutiveReportsPage extends ConstantsMainPage{

	public static final By SELECT_BTN = By.className("dropdown-toggle");
	public static final By ALL_SELECT_BTN = By.cssSelector("a[data-ng-click='selectAll()']");
	public static final By SELECT_COMPARASION = By.name("comparasion");
	public static final By VALUE = By.name("value");
	public static final By RESULT_BTN = By.className("buttonBlack");
}
