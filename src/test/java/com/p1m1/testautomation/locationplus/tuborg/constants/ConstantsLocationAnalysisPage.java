package com.p1m1.testautomation.locationplus.tuborg.constants;

import org.openqa.selenium.By;

/**
 * Location Analysis Constants
 * 
 * @author emre.orhan
 *
 */

public class ConstantsLocationAnalysisPage extends ConstantsMainPage {

	public static final By ADD_LOCATION_BTN = By.id("btnGeoAddLocation");
	public static final By DELETE_LOCATION_BTN = By.id("btnDeleteLocation");
	public static final By ADD_NEW_LOCATION_BTN = By.id("btnAddNewLocation");
	public static final By BEST_LOCATIONS_BTN = By.id("btnBestLocations");
	/**
	 * TABS
	 */
	public static final By TAB_GENERAL_INFORMATION = By.linkText("Genel Bilgiler");
	public static final By TAB_DETAIL_INFORMATION = By.linkText("Detay Bilgiler");
	public static final By TAB_SALES_INFORMATION = By.linkText("Satış Bilgiler");
	public static final By TAB_CABINET_TYPE = By.linkText("Dolap Türü");
	/**
	 * TABS
	 */
	/**
	 * GENERAL INFORMATION TAB
	 */
	public static final By CUSTOMER_ID = By.id("CustomerID");
	public static final By REGION_SELECT_BOX = By.cssSelector("label[for='Region'] ~ .ng-scope select");
	public static final By SEGMENT_SELECT_BOX = By.cssSelector("label[for='Segment'] ~ .ng-scope select");
	public static final By ACTIVEPASSIVE_SELECT_BOX = By.cssSelector("label[for='ActivePassive'] ~ .ng-scope select");
	public static final By AGENT_SELECT_BOX = By.cssSelector("label[for='Agent'] ~ .ng-scope select");
	public static final By FRIDGESTATUS_SELECT_BOX = By.cssSelector("label[for='FridgeStatus'] ~ .ng-scope select");
	public static final By CUSTOMER_NAME = By.id("CustomerName");
	public static final By VENDORNAME_SELECT_BOX = By.cssSelector("label[for='VendorName'] ~ .ng-scope select");
	public static final By CUSTOMERTYPE_SELECT_BOX = By.cssSelector("label[for='CustomerType'] ~ .ng-scope select");
	public static final By ADMIN_SELECT_BOX = By.cssSelector("label[for='Admin'] ~ .ng-scope select");
	public static final By SALESTYPE_SELECT_BOX = By.cssSelector("label[for='SalesType'] ~ .ng-scope select");
	/**
	 * GENERAL INFORMATION TAB
	 */
	/**
	 * CABINET TYPE
	 */
	public static final By CABINET_TYPE_1MTDOUBLEDOOR = By.id("1mtDoubleDoor");
	public static final By CABINET_TYPE_ONEANDHALFDOOR = By.id("OneAndHalfDoor");
	public static final By CABINET_TYPE_POOLTYPE = By.id("PoolType");
	public static final By CABINET_TYPE_OPENFRONT = By.id("OpenFront");
	public static final By CABINET_TYPE_SEMI = By.id("Semi");
	public static final By CABINET_TYPE_ONEDOOR = By.id("OneDoor");
	public static final By CABINET_TYPE_HORIZONTAL = By.id("Horizontal");
	public static final By CABINET_TYPE_BACKBAR = By.id("BackBar");
	public static final By CABINET_TYPE_DOUBLEDOOR = By.id("DoubleDoor");
	public static final By CABINET_TYPE_MINIBAR = By.id("MiniBar");
	public static final By CABINET_TYPE_RETRO = By.id("Retro");
	public static final By CABINET_TYPE_SLIM = By.id("Slim");
	public static final By CABINET_TYPE_TRIPLEDOOR = By.id("TripleDoor");
	public static final By CABINET_TYPE_INCASECOOLER = By.id("KasaOnuSogutucu");
	/**
	 * CABINET TYPE
	 */

	/**
	 * LOCATION BUTTONS
	 */
	public static final By SAVE_BTN = By.cssSelector("btnAddNode");
	public static final By UPDATE_BTN = By.cssSelector("btnUpdateNode");
	public static final By CANCEL_BTN = By.cssSelector("btnCancelBack");
	/**
	 * LOCATION BUTTONS
	 */
	public static final By FILTER_AREA = By.cssSelector(".ui-grid-filter-input.ui-grid-filter-input-0");
	public static final By FILTER_ROW = By.className("ui-grid-row");
}
