package com.p1m1.testautomation.locationplus.tuborg.constants;

import org.openqa.selenium.By;

/**
 * @author emreorhan
 *
 */
public class ConstantsLoginPage extends ConstantsMainPage {

	// id olanlar release çıktığında geri alınacak.
	public static final By LOGINBOX = By.name("loginForm");
	// public static final By LOGIN_EMAIL = By.id("txtUserEmail")
	public static final By LOGIN_EMAIL = By.name("userEmail");
	// public static final By LOGIN_PASSWORD = By.id("txtUserPassword")
	public static final By LOGIN_PASSWORD = By.name("userPassword");
	// public static final By LOGIN_BTN = By.id("btnSignIn")
	public static final By LOGIN_BTN = By.className("btn-lg");

	/**
	 * ERROR EVENTS
	 */
	public static final By ERROR_MESSAGE_BOX = By.className("modal-dialog");
	public static final By ERROR_MESSAGE_TXT = By.cssSelector(".modal-dialog .modal-body .ng-binding");
	/**
	 * ERROR EVENTS
	 */
}
