package com.p1m1.testautomation.locationplus.tuborg.constants;

import org.openqa.selenium.By;

import com.p1m1.testautomation.locationplus.util.Configuration;

/**
 * @author emreorhan
 *
 */
public class ConstantsMainPage {
	protected static Configuration config = Configuration.getInstance();

	/**
	 * MainPage Constants
	 */
	public static final By LOGO = By.cssSelector("#logo a");
	public static final By PACE_RUNNIG = By.className("pace-runnig");
	public static final By LOADING_BAR = By.className(".block-ui-message.ng-binding");
	public static final By HOME_BTN = By.className("glyphicon-home");
	public static final By CLOSE_BTN = By.className("glyphicon-log-out");
	/**
	 * MainPage Constants
	 */
	/**
	 * MODULES
	 */
	public static final By DATA_ANALYZE_BTN = By.linkText("Veri Analizi");
	public static final By LOCATION_ANALYZE_BTN = By.linkText("Lokasyon Analizi");
	public static final By CREATE_AREA_BTN = By.linkText("Alan Oluşturma");
	public static final By NEXT_BTN = By.id("btnNext");
	public static final By MODULE_BOX = By.cssSelector("div.moduleBox div img");
	public static final By EXECUTIVE_REPORTS_BTN = By.linkText("Yönetici Raporları");
	public static final By ANALYSİS_REPORTS_BTN = By.linkText("Analiz Raporları");
	/**
	 * MODULES
	 */
	public static final By SELECT_COUNTRY = By
			.cssSelector("div[ng-grid='gridCounties'] div.ngCellText span.ng-binding");
	public static final By SELECT_AREA = By.cssSelector("div[ng-grid='gridAreas'] div.ngCellText span.ng-binding");
	public static final By SELECT_CITY = By.cssSelector("div[ng-grid='gridCities'] div.ngCellText span.ng-binding");
	public static final By SELECTED_AREAS = By
			.cssSelector("div[ng-grid='gridSelectedAreas'] div.ngCellText span.ng-binding");

	public static final By LAYERS_TOGGLE = By.className("leaflet-control-layers-toggle");
	public static final By LAYER_TEK_TT = By.id("tek-tt");
	public static final By LAYER_TEK_EP = By.id("tek-ep");
	public static final By LAYER_KARISIK = By.id("karisik");
	public static final By LAYER_AVM = By.id("avm");
	public static final By LAYER_CAMI = By.id("cami");
	public static final By LAYER_DINI_TESIS_ETKI_ALANI = By.id("dini-tesis-etki-alani");
	public static final By LAYER_EGITIM_KURUMLARI = By.id("egitim-kurumlari");
	public static final By LAYER_EGITIM_KURUMLARI_ETKI_ALANI = By.id("egitim-kurumlari-etki-alani");
	public static final By LAYER_EGLENCE_MERKEZI = By.id("eglence-merkezi");
	public static final By LAYER_FUAR_MERKEZI = By.id("fuar-merkezi");
	public static final By LAYER_HAVAALANI = By.id("havaalani");
	public static final By LAYER_IS_MERKEZLERI = By.id("is-merkezleri");
	public static final By LAYER_MESIRE_YERLERI = By.id("mesire-yerleri");
	public static final By LAYER_PLAJ = By.id("plaj");
	public static final By LAYER_REKREASYON_ALANLARI = By.id("rekreasyon-alanlari");
	public static final By LAYER_DEVLET_UNIVERSITESI = By.id("devlet-universitesi");
	public static final By LAYER_VAKIF_UNIVERSITESI = By.id("vakif-(ozel)-universitesi");
	public static final By LAYER_YENI_LOKASYONLAR = By.id("yeni-lokasyonlar");
	public static final By LAYER_BOLGE_SINIRI = By.id("bolge-sinirı");
	public static final By LAYER_SORGU_SONUCU = By.id("sorgu-sonucu");
	public static final By LAYER_NOKTASAL_SORGU_SONUCU = By.id("noktasal-sorgu-sonucu");
	public static final By LAYER_GROUP1_TAHMIN = By.id("group1_tahmin");
	public static final By LAYER_GROUP2_TAHMIN = By.id("group2_tahmin");
	public static final By LAYER_GROUP3_TAHMIN = By.id("group3_tahmin");
	public static final By LAYER_GROUP4_TAHMIN = By.id("group4_tahmin");
	public static final By LAYER_GROUP5_TAHMIN = By.id("group5_tahmin");
	public static final By LAYER_GROUP6_TAHMIN = By.id("group6_tahmin");
	public static final By LAYER_NOKTA_GOSTERIMI = By.id("nokta-gosterimi");
	public static final By LAYER_ON_PREMISE = By.id("on-premise");
	public static final By LAYER_OFF_PREMISE = By.id("off-premise");
	public static final By LAYER_KEY_ACCOUNT = By.id("key-account");
	public static final By LAYER_BAYILER = By.id("bayiler");
	public static final By LAYER_DOLAPLI_LOKASYONLAR = By.id("dolapli-lokasyonlar");
	public static final By LAYER_DOLAPSIZ_LOKASYONLAR = By.id("dolapsiz-lokasyonlar");
	public static final By LAYER_AYLIK_HEDEF_BASARILI = By.id("aylik-hedef-basarilı");
	public static final By LAYER_AYLIK_HEDEF_BASARISIZ = By.id("aylik-hedef-basarisız");
	
	public static final By MAP = By.id("panelMap");
	public static final By MAP_ZOOM_IN = By.className("leaflet-control-zoom-in");
	public static final By MAP_ZOOM_OUT = By.className("leaflet-control-zoom-out");
	public static final By MAP_POPUP_BOX = By.className("leaflet-popup-content");

	protected ConstantsMainPage() {
	}
}
