package com.p1m1.testautomation.locationplus.tuborg.page;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import static com.p1m1.testautomation.locationplus.tuborg.constants.ConstantsMainPage.*;
import static com.p1m1.testautomation.locationplus.tuborg.constants.ConstantsAnalysisReportsPage.*;

import com.p1m1.testautomation.locationplus.base.BasePage;

public class AnalysisReportsPage extends BasePage {
	protected final Logger logger = Logger.getLogger(AnalysisReportsPage.class);

	public AnalysisReportsPage(WebDriver driver) {
		super(driver);
	}

	/**
	 * @return
	 */
	public AnalysisReportsPage openReports() {
		untilElementDisappear(LOADING_BAR);
		clickElement(MODULE_BOX, 1);
		return this;
	}

	/**
	 * @return
	 */
	public AnalysisReportsPage openAnalysisReports() {
		openReports();
		clickElement(ANALYSİS_REPORTS_BTN);
		return this;
	}

	/**
	 * @param analysisName
	 * @return
	 */
	public AnalysisReportsPage selectAnalysisReport(String analysisName) {
		selectOptionClick(SELECT_ANALYSIS_REPORT, analysisName);
		return this;
	}

	/**
	 * @return
	 */
	public AnalysisReportsPage clickLoadAnalysis() {
		clickElement(LOAD_ANALYSIS_BTN);
		untilElementDisappear(LOADING_BAR);
		return this;
	}

	/**
	 * @param customerName
	 * @return
	 */
	public AnalysisReportsPage selectCustomerName(String customerName) {
		String generateCustomerName = "_" + customerName;
		By customerNameClass = By.className(generateCustomerName);
		untilElementDisappear(LOADING_BAR);
		waitForElementPresent(customerNameClass);
		clickElement(customerNameClass);
		untilElementDisappear(LOADING_BAR);
		return this;
	}

	/**
	 * @return
	 * @throws InterruptedException
	 */
	public AnalysisReportsPage clickDrawGraphic() throws InterruptedException {
		untilElementDisappear(LOADING_BAR);
		timeUnitSeconds(2);
		clickElement(DRAW_GRAPHIC_BTN);
		untilElementDisappear(LOADING_BAR);
		timeUnitSeconds(3);
		if (isElementDisplayed(GRAPHICS)) {
			logger.info("The graphic was drawn successfully.");
		} else {
			assertFail("Graph not drawn");
		}
		return this;
	}
}
