package com.p1m1.testautomation.locationplus.tuborg.page;

import static com.p1m1.testautomation.locationplus.tuborg.constants.ConstantsDataAnalysisPage.*;
import static com.p1m1.testautomation.locationplus.tuborg.constants.ConstantsMainPage.LOADING_BAR;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.p1m1.testautomation.locationplus.base.BasePage;

public class DataAnalysisPage extends BasePage {
	protected final Logger logger = Logger.getLogger(DataAnalysisPage.class);
	static String outerText = "outerText";

	public DataAnalysisPage(WebDriver driver) {
		super(driver);
	}

	/**
	 * The menus also search according to the entered data and perform the
	 * clicking process.
	 * 
	 * @param by
	 * @param text
	 */
	protected void searchAndClick(By by, String text) {
		for (WebElement selected : findElements(by)) {
			logger.info(selected.getAttribute(outerText));
			if (text.equals(selected.getAttribute(outerText))) {
				untilElementDisappear(LOADING_BAR);
				clickElement(selected, false);
			}
		}
	}

	/**
	 * Opens the Data Analysis field.
	 * 
	 * @return
	 */
	public DataAnalysisPage openDataAnalysis() {
		untilElementDisappear(LOADING_BAR);
		clickElement(DATA_ANALYZE_BTN);
		return this;
	}

	/**
	 * Country and Region Select.
	 * 
	 * @param area
	 * @return
	 */
	public DataAnalysisPage selectArea(String area) {
		untilElementAppear(SELECT_COUNTRY);
		untilElementDisappear(LOADING_BAR);
		clickElement(SELECT_COUNTRY);
		untilElementDisappear(LOADING_BAR);
		untilElementAppear(SELECT_AREA);
		searchAndClick(SELECT_AREA, area);
		return this;
	}

	/**
	 * The dealer performs the selection.
	 * 
	 * @param city
	 * @return
	 */
	public DataAnalysisPage selectCity(String city) {
		untilElementDisappear(LOADING_BAR);
		untilElementAppear(SELECT_CITY);
		searchAndClick(SELECT_CITY, city);
		untilElementDisappear(LOADING_BAR);
		return this;
	}

	/**
	 * Performs the selected Dealer Control.
	 * 
	 * addLocation() Should be used after the method.
	 * 
	 * @param city
	 *            = City name entry
	 * @return
	 */
	public DataAnalysisPage addLocationControl(String city) {
		untilElementDisappear(LOADING_BAR);
		untilElementAppear(SELECTED_AREAS);
		for (WebElement selectedCity : findElements(SELECTED_AREAS)) {
			logger.info(selectedCity.getAttribute(outerText));
			if (city.equals(selectedCity.getAttribute(outerText))) {
				logger.info("Specified area is properly selected.");
			} else {
				assertFail("The specified area could not be properly formatted.");
			}
		}
		return this;
	}

	/**
	 * Complete location addition process.
	 * 
	 * @return
	 */
	public DataAnalysisPage addLocation() {
		// Ekle Butonuna Basılıyor.
		clickElement(ADD_LOCATION_BTN);
		untilElementDisappear(LOADING_BAR);
		return this;
	}

	/**
	 * Pressing the forward button allows progression.
	 * 
	 * @return
	 */
	public DataAnalysisPage nextClick() {
		// İleri Butonuna Basılıyor.
		clickElement(NEXT_BTN);
		untilElementDisappear(LOADING_BAR);
		untilElementAppear(REFERENCEDATA_SELECTBOX);
		return this;
	}

	/**
	 * Reference Selects the desired value in the data field.
	 * 
	 * @param referenceData
	 * @return
	 */
	public DataAnalysisPage selectRefenceData(String referenceData) {
		logger.info("The Reference Data field is selected. Area to be selected :" + referenceData);
		selectOptionClick(REFERENCEDATA_SELECTBOX, referenceData);
		logger.info("The Reference Data field is selected. Selected area :" + referenceData);
		return this;
	}

	/**
	 * Selects the desired value in the Query Column section.
	 * 
	 * @param queryColumn
	 * @return
	 */
	public DataAnalysisPage selectQueryColumn(String queryColumn) {
		logger.info("Query Column is selected. Column to be selected :" + queryColumn);
		selectOptionClick(QUERY_COLUMN_SELECTBOX, queryColumn);
		logger.info("Query Column selected. Selected area :" + queryColumn);
		return this;
	}

	/**
	 * The criterion field also selects the desired value.
	 * 
	 * @param criteriaColumn
	 * @return
	 */
	public DataAnalysisPage selectCriteriaColumn(String criteriaColumn) {
		logger.info("Criterion Field is selected. Area to be selected :" + criteriaColumn);
		selectOptionClick(CRITERIA_COLUMN_SELECTBOX, criteriaColumn);
		logger.info("Criteria Field was selected. Selected area :" + criteriaColumn);
		return this;
	}

	/**
	 * Fills the value field with the desired value.
	 * 
	 * @param value
	 * @return
	 */
	public DataAnalysisPage selectValue(String like, String value) {
		// Wait for element dissapear
		untilElementDisappear(LOADING_BAR);
		if ("like".equals(like)) {
			selectOptionClick(VALUE_COLUMN_SELECTBOX, value);
		} else {
			logger.info("Value data is entered. Value entered :" + value);
			fillInputField(VALUE_TEXT_BOX, value);
		}
		return this;
	}

	/**
	 * Click Add Query button.
	 * 
	 * @return
	 */
	public DataAnalysisPage clickAddQuery(String queryType) {
		logger.info("Clicking on the Add Query button.");
		untilElementDisappear(LOADING_BAR);
		untilLoadinBarDissapear();
		if ("VE".equals(queryType) || "AND".equals(queryType)) {
			logger.info("Clicking on the 'AND' button.");
			clickElement(AND_BTN);
		} else if ("VEYA".equals(queryType) || "OR".equals(queryType)) {
			logger.info("Clicking on the 'OR' button.");
			clickElement(OR_BTN);
		} else {
			logger.info("NO QUERY TYPE SELECTED...");
		}
		assertionEquals("The selected does not match the one desired.", getAttributeValue(AND_OR_TXT).toLowerCase(),
				queryType.toLowerCase());
		clickElement(ADD_QUERY_BTN);
		untilElementDisappear(LOADING_BAR);
		return this;
	}

	/**
	 * Click on the "Query" button.
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public DataAnalysisPage clickQuery() throws InterruptedException {
		logger.info("The query button is pressed.");
		// The query button is clicked.
		clickElement(SEARCH_QUERY_BTN);
		// Loading bar is expected to disappear.
		untilElementDisappear(LOADING_BAR);
		logger.info("Query button pressed.");
		//Waiting 1 seconds for the effects to disappear.
		timeUnitSeconds(1);
		return this;
	}

	/**
	 * The marker on the map is clicked.
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public DataAnalysisPage clickMarker() throws InterruptedException {
		// Loading bar is expected to disappear.
		untilElementDisappear(LOADING_BAR);
		// Zoom is in progress
		clickElement(MAP_ZOOM_IN);
		timeUnitSeconds(1);
		// Zoom is in progress
		clickElement(MAP_ZOOM_IN);
		// The first marker is clicked.
		clickElementJS(MAP_MARKER, 0);
		// Popup box is expected to open.
		waitForElementPresent(MAP_POPUP_BOX);
		//Waiting 2 seconds for the effects to disappear.
		timeUnitSeconds(2);
		logger.info("Popup displayed.");
		return this;
	}

	/**
	 * Moving onto the Layers field
	 * 
	 * @return
	 */
	public DataAnalysisPage hoverLayersToggle() {
		// Loading bar is expected to disappear.
		untilElementDisappear(LOADING_BAR);
		// Moving onto the Layers field
		hoverElementJS(LAYERS_TOGGLE);
		return this;
	}

	/**
	 * Selected layers are removed as standard.
	 * 
	 * @return
	 */
	public DataAnalysisPage uncheckedStandartLayers() {
		// Moving onto the Layers field
		hoverLayersToggle();
		// Loading bar is expected to disappear.
		untilElementDisappear(LOADING_BAR);
		// Selected layers are removed as standard.
		clickElement(LAYER_TEK_TT);
		untilElementDisappear(LOADING_BAR);
		clickElement(LAYER_TEK_EP);
		untilElementDisappear(LOADING_BAR);
		clickElement(LAYER_KARISIK);
		untilElementDisappear(LOADING_BAR);
		clickElement(LAYER_YENI_LOKASYONLAR);
		untilElementDisappear(LOADING_BAR);
		return this;
	}

	/**
	 * The data in the popup is checked.
	 * 
	 * @param criteriaData
	 * @param queryData
	 * @param value
	 * @return
	 */
	public DataAnalysisPage checkPopupData(String criteriaData, String queryData, String value) {
		if ("like".equals(criteriaData)) {
			assertionTrue("Values Do Not Match", isTextContains(MAP_POPUP_BOX, queryData + " = " + value));
		} else {
			assertionTrue("Values Do Not Match", isTextContains(MAP_POPUP_BOX, queryData + " = "));
		}
		return this;
	}
}
