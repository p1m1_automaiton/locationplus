package com.p1m1.testautomation.locationplus.tuborg.page;

import static com.p1m1.testautomation.locationplus.tuborg.constants.ConstantsExecutiveReportsPage.*;
import static com.p1m1.testautomation.locationplus.tuborg.constants.ConstantsMainPage.EXECUTIVE_REPORTS_BTN;
import static com.p1m1.testautomation.locationplus.tuborg.constants.ConstantsMainPage.LOADING_BAR;
import static com.p1m1.testautomation.locationplus.tuborg.constants.ConstantsMainPage.MODULE_BOX;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.p1m1.testautomation.locationplus.base.BasePage;

public class ExecutiveReportsPage extends BasePage {
	protected final Logger logger = Logger.getLogger(ExecutiveReportsPage.class);

	public ExecutiveReportsPage(WebDriver driver) {
		super(driver);
	}

	/**
	 * Reports tab is open.
	 * 
	 * @return
	 */
	public ExecutiveReportsPage openReports() {
		untilElementDisappear(LOADING_BAR);
		clickElementJS(MODULE_BOX, 1);
		return this;
	}

	/**
	 * Checking that page url belongs to admin reports.
	 * 
	 * @return
	 */
	public ExecutiveReportsPage checkExecutiveUrl() {
		assertionTrue("Page url address does not match", getCurrentUrl().contains("ExecutiveDashboardService"));
		return this;
	}

	/**
	 * @return
	 * @throws InterruptedException
	 */
	public ExecutiveReportsPage openExecutiveReports() throws InterruptedException {
		openReports();
		timeUnitSeconds(1);
		if (isElementDisplayed(EXECUTIVE_REPORTS_BTN)) {
			clickElement(EXECUTIVE_REPORTS_BTN);
		} else {
			assertFail("The user is not authorized");
		}
		return this;
	}

	/**
	 * All regions are selected.
	 * 
	 * @param area
	 * @return
	 * @throws InterruptedException
	 */
	public ExecutiveReportsPage selectAllArea() throws InterruptedException {
		untilElementDisappear(LOADING_BAR);
		clickElement(SELECT_BTN, 0);
		timeUnitSeconds(1);
		clickElement(ALL_SELECT_BTN, 0);
		untilElementDisappear(LOADING_BAR);
		untilElementDisappear(LOADING_BAR);
		clickElement(SELECT_BTN, 0);
		return this;
	}

	/**
	 * All dealers are selected.
	 * 
	 * @param area
	 * @return
	 * @throws InterruptedException
	 */
	public ExecutiveReportsPage selectAllDealer() throws InterruptedException {
		untilElementDisappear(LOADING_BAR);
		clickElement(SELECT_BTN, 1);
		timeUnitSeconds(1);
		clickElement(ALL_SELECT_BTN, 1);
		untilElementDisappear(LOADING_BAR);
		untilElementDisappear(LOADING_BAR);
		clickElement(SELECT_BTN, 1);
		return this;
	}

	/**
	 * All types are selected.
	 * 
	 * @param area
	 * @return
	 * @throws InterruptedException
	 */
	public ExecutiveReportsPage selectAllType() throws InterruptedException {
		untilElementDisappear(LOADING_BAR);
		clickElement(SELECT_BTN, 2);
		timeUnitSeconds(1);
		clickElement(ALL_SELECT_BTN, 2);
		untilElementDisappear(LOADING_BAR);
		untilElementDisappear(LOADING_BAR);
		clickElement(SELECT_BTN, 2);
		return this;
	}

	/**
	 * All segments are selected.
	 * 
	 * @param area
	 * @return
	 * @throws InterruptedException
	 */
	public ExecutiveReportsPage selectAllSegment() throws InterruptedException {
		untilElementDisappear(LOADING_BAR);
		clickElement(SELECT_BTN, 3);
		timeUnitSeconds(1);
		clickElement(ALL_SELECT_BTN, 3);
		untilElementDisappear(LOADING_BAR);
		untilElementDisappear(LOADING_BAR);
		clickElement(SELECT_BTN, 3);
		return this;
	}

	/**
	 * The comparison is selected.
	 * 
	 * @param comparasion
	 * @return
	 */
	public ExecutiveReportsPage selectComparasion(String comparasion) {
		logger.info("Selected comparison :" + comparasion);
		selectOptionClick(SELECT_COMPARASION, comparasion);
		return this;
	}

	/**
	 * Value field is being filled
	 * 
	 * @param value
	 * @return
	 */
	public ExecutiveReportsPage fillValueField(String value) {
		fillInputField(VALUE, value);
		return this;
	}

	/**
	 * Click on the fetch results button.
	 * 
	 * @return
	 * @throws InterruptedException 
	 */
	public ExecutiveReportsPage clickGetResult() throws InterruptedException {
		clickElement(RESULT_BTN);
		untilElementDisappear(LOADING_BAR);
		timeUnitSeconds(5);
		return this;
	}
}
