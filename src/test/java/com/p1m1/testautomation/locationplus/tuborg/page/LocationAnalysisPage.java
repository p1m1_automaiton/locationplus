package com.p1m1.testautomation.locationplus.tuborg.page;

import static com.p1m1.testautomation.locationplus.tuborg.constants.ConstantsDataAnalysisPage.MAP_MARKER;
import static com.p1m1.testautomation.locationplus.tuborg.constants.ConstantsLocationAnalysisPage.*;
import static com.p1m1.testautomation.locationplus.tuborg.constants.ConstantsMainPage.LOADING_BAR;
import static com.p1m1.testautomation.locationplus.tuborg.constants.ConstantsMainPage.MAP_POPUP_BOX;
import static com.p1m1.testautomation.locationplus.tuborg.constants.ConstantsMainPage.MAP_ZOOM_IN;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.p1m1.testautomation.locationplus.base.BasePage;

public class LocationAnalysisPage extends BasePage {
	protected final Logger logger = Logger.getLogger(LocationAnalysisPage.class);
	static String outerText = "outerText";

	public LocationAnalysisPage(WebDriver driver) {
		super(driver);
	}

	/**
	 * The menus also search according to the entered data and perform the
	 * clicking process.
	 * 
	 * @param by
	 * @param text
	 */
	protected void searchAndClick(By by, String text) {
		for (WebElement selected : findElements(by)) {
			logger.info(selected.getAttribute(outerText));
			if (text.equals(selected.getAttribute(outerText))) {
				untilElementDisappear(LOADING_BAR);
				clickElement(selected, false);
			}
		}
	}

	/**
	 * Opens the Location Analysis field.
	 * 
	 * @return
	 */
	public LocationAnalysisPage openLocationAnalysis() {
		untilElementDisappear(LOADING_BAR);
		clickElement(LOCATION_ANALYZE_BTN);
		return this;
	}

	/**
	 * Country and Region Select.
	 * 
	 * @param area
	 * @return
	 */
	public LocationAnalysisPage selectArea(String area) {
		untilElementAppear(SELECT_COUNTRY);
		untilElementDisappear(LOADING_BAR);
		clickElement(SELECT_COUNTRY);
		untilElementDisappear(LOADING_BAR);
		untilElementAppear(SELECT_AREA);
		searchAndClick(SELECT_AREA, area);
		return this;
	}

	/**
	 * The dealer performs the selection.
	 * 
	 * @param city
	 * @return
	 */
	public LocationAnalysisPage selectLocation(String city) {
		untilElementDisappear(LOADING_BAR);
		untilElementAppear(SELECT_CITY);
		searchAndClick(SELECT_CITY, city);
		untilElementDisappear(LOADING_BAR);
		return this;
	}

	/**
	 * Performs the selected Dealer Control.
	 * 
	 * addLocation() Should be used after the method.
	 * 
	 * @param city
	 *            = City name entry
	 * @return
	 */
	public LocationAnalysisPage addLocationControl(String city) {
		untilElementDisappear(LOADING_BAR);
		untilElementAppear(SELECTED_AREAS);
		for (WebElement selectedCity : findElements(SELECTED_AREAS)) {
			logger.info(selectedCity.getAttribute(outerText));
			if (city.equals(selectedCity.getAttribute(outerText))) {
				logger.info("Specified area is properly selected.");
			} else {
				logger.error("*** The specified area is not properly formatted");
				assertFail("The specified area is not properly formatted.");
			}

		}
		return this;
	}

	/**
	 * Complete location addition process.
	 * 
	 * @return
	 */
	public LocationAnalysisPage addLocation() {
		// Pressing "Add" button.
		clickElement(ADD_LOCATION_BTN);
		untilElementDisappear(LOADING_BAR);
		return this;
	}

	/**
	 * Pressing the forward button allows progression.
	 * 
	 * @return
	 */
	public LocationAnalysisPage nextClick() {
		// Pressing "Next" Button.
		clickElement(NEXT_BTN);
		untilElementDisappear(LOADING_BAR);
		untilElementAppear(FILTER_AREA);
		return this;
	}

	/**
	 * Click "Add New Location" button.
	 * 
	 * @return
	 */
	public LocationAnalysisPage addNewLocationClick() {
		// Pressing "Add New Location" button.
		clickElement(ADD_NEW_LOCATION_BTN);
		return this;
	}

	/**
	 * The "Best Locations" click on the button.
	 * 
	 * @return
	 */
	public LocationAnalysisPage bestLocationsClick() {
		// Suggestions are being pressed on the button.
		clickElement(BEST_LOCATIONS_BTN);
		return this;
	}

	/**
	 * The marker on the map is clicked.
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public LocationAnalysisPage clickMarker() throws InterruptedException {
		// Loading bar is expected to disappear.
		untilElementDisappear(LOADING_BAR);
		// Zoom is in progress
		clickElement(MAP_ZOOM_IN);
		timeUnitSeconds(1);
		// Zoom is in progress
		clickElement(MAP_ZOOM_IN);
		// The first marker is clicked.
		clickElementJS(MAP_MARKER, 0);
		// Popup box is expected to open.
		waitForElementPresent(MAP_POPUP_BOX);
		// Waiting 2 seconds for the effects to disappear.
		timeUnitSeconds(2);
		logger.info("Popup displayed.");
		return this;
	}

	/**
	 * Performs a search by customer name.
	 * 
	 * @param customerName
	 * @return
	 * @throws InterruptedException
	 */
	public LocationAnalysisPage searchCustomerName(String customerName) throws InterruptedException {
		fillInputField(FILTER_AREA, customerName, 1);
		timeUnitSeconds(2);
		if (isElementDisplayed(FILTER_ROW, 0)) {
			clickElement(FILTER_ROW, 0);
			waitForElementPresent(MAP_POPUP_BOX);
		} else {
			assertFail("The requested customer name could not be found.");
		}
		return this;
	}
}
