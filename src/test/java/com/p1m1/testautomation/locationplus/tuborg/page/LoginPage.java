package com.p1m1.testautomation.locationplus.tuborg.page;

import static com.p1m1.testautomation.locationplus.tuborg.constants.ConstantsLoginPage.*;
import static com.p1m1.testautomation.locationplus.tuborg.constants.ConstantsMainPage.CLOSE_BTN;
import static com.p1m1.testautomation.locationplus.tuborg.constants.ConstantsMainPage.HOME_BTN;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.p1m1.testautomation.locationplus.base.BasePage;

public class LoginPage extends BasePage {
	protected final Logger logger = Logger.getLogger(LoginPage.class);
	String eposta = "tuborglocation";
	String hataliEposta = "tbrglocation";
	String pew = "1";

	public LoginPage(WebDriver driver) {
		super(driver);
	}

	/**
	 * Includes steps to login.
	 * 
	 * @param email
	 * @param pw
	 * @return
	 */
	public LoginPage loginSteps(String email, String pw) {
		logger.info("Login Box checked.");
		// The login box is expected to arrive.
		untilElementAppear(LOGINBOX);
		// The email field is being filled.
		logger.info("User name is entered.");
		fillInputField(LOGIN_EMAIL, email);
		// Password field filled in.
		logger.info("Password is entered.");
		fillInputField(LOGIN_PASSWORD, pw);
		return this;
	}

	/**
	 * Click on the "Sign In" button.
	 * 
	 * @return
	 */
	public LoginPage signBtn() {
		// Clicking on Login button.
		logger.info("Clicking on the 'Login' button.");
		clickElement(LOGIN_BTN);
		// The loading bar is expected to disappear.
		untilAjaxComplete();
		return this;
	}

	/**
	 * Succes Login Control
	 * 
	 * @return
	 */
	public LoginPage succesLoginControl(String user, String pw) {
		// Perform login steps and click on the login button.
		loginSteps(user, pw).signBtn();
		if (isElementDisplayed(ERROR_MESSAGE_BOX)) {
			assertFail(getText(ERROR_MESSAGE_TXT));
		}
		// The loading bar is expected to disappear.
		untilAjaxComplete();
		// Checking is being done.
		waitForElementPresent(HOME_BTN);
		logger.info("Home and Exit Buttons are checked");
		assertionTrue("Home button not displayed or not logged in.", isElementDisplayed(HOME_BTN));
		assertionTrue("The Exit button may not be displayed or may not be entered.", isElementDisplayed(CLOSE_BTN));
		return this;
	}

	/**
	 * Failed Login Control
	 * 
	 * @return
	 */
	public LoginPage failLoginControl() {
		// Perform login steps and click on the login button.
		loginSteps(hataliEposta, pew).signBtn();
		waitForElementPresentB(ERROR_MESSAGE_BOX);
		assertionTrue(
				"The desired message was not displayed after faulty operation. Message requested :" + getText(ERROR_MESSAGE_TXT),
				isTextContains(ERROR_MESSAGE_TXT, "Common.Error.INLAVID_USERNAME_PASSWORD", 0));
		logger.info("FAULT MESSAGE SUCCESSFUL VIEWING.");
		return this;
	}
}
