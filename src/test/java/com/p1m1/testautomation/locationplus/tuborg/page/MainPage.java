package com.p1m1.testautomation.locationplus.tuborg.page;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.p1m1.testautomation.locationplus.base.BasePage;

public class MainPage extends BasePage {

	protected final Logger logger = Logger.getLogger(MainPage.class);

	public MainPage(WebDriver driver) {
		super(driver);
	}

	/**
	 * @return
	 */
	public MainPage checkPageUrl() {
		logger.info("Found page url :" + getCurrentUrl());
		logger.info("Search page url :" + config.getServerUrl());
		assertionTrue("The page address and the server url do not match",
				getCurrentUrl().contains(config.getServerUrl()));
		return new MainPage(driver);
	}

	/**
	 * Navigate to homepage control
	 * 
	 * @return
	 */
	public MainPage navigate() {
		navigateTo("http://www.p1m1.com");
		return new MainPage(driver);
	}

}
