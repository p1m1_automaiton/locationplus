package com.p1m1.testautomation.locationplus.tuborg.test;

import static com.p1m1.testautomation.locationplus.util.ExcelUtils.getTableArray;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.p1m1.testautomation.locationplus.base.BaseTest;
import com.p1m1.testautomation.locationplus.tuborg.page.AnalysisReportsPage;
import com.p1m1.testautomation.locationplus.tuborg.page.LoginPage;

public class AnalysisReportsPageTest extends BaseTest {
	protected final Logger logger = Logger.getLogger(AnalysisReportsPageTest.class);
	AnalysisReportsPage arp;
	LoginPage lp;

	@DataProvider(name = "analysisReportsData")
	public Object[][] createLocations() {
		return getTableArray("properties/locationPlusExcel/"+getExcelName(), "analysisReports", "analysisReportsData");
	}

	@BeforeMethod
	public void before() throws InterruptedException {
		logger.info("Driver open...");
		lp = new LoginPage(getDriver());
		arp = new AnalysisReportsPage(getDriver());
		logger.info("Test started");
	}

	@Test(description = "Analysis Reports Test", dataProvider = "analysisReportsData")
	public void analysisReportsTest(String user, String pw, String analysis, String customerName)
			throws InterruptedException {
		try {
			lp.succesLoginControl(user, pw);

			arp.openAnalysisReports().selectAnalysisReport(analysis).clickLoadAnalysis()
					.selectCustomerName(customerName).clickDrawGraphic();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			Assert.fail(e.getMessage());
		}
	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {
		getDriver().close();
		logger.info("Test Finished...");
	}
}
