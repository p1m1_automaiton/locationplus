package com.p1m1.testautomation.locationplus.tuborg.test;

import static com.p1m1.testautomation.locationplus.util.ExcelUtils.getTableArray;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.p1m1.testautomation.locationplus.base.BaseTest;
import com.p1m1.testautomation.locationplus.tuborg.page.DataAnalysisPage;
import com.p1m1.testautomation.locationplus.tuborg.page.LoginPage;

public class DataAnalysisPageTest extends BaseTest {
	protected final Logger logger = Logger.getLogger(DataAnalysisPageTest.class);
	DataAnalysisPage dap;
	LoginPage lp;

	@DataProvider(name = "dataAnalysisData")
	public Object[][] createLocations() {
		return getTableArray("properties/locationPlusExcel/"+getExcelName(), "dataAnalysis", "dataAnalysisData");
	}

	@BeforeMethod
	public void before() throws InterruptedException {
		logger.info("Driver open...");
		lp = new LoginPage(getDriver());
		dap = new DataAnalysisPage(getDriver());
		logger.info("Test started");
	}

	@Test(description = "DataAnalysis", dataProvider = "dataAnalysisData")
	public void dataAnalyzeControlTest(String user, String password, String area, String city, String location,
			String referenceData, String queryData, String criteriaData, String value, String queryType)
			throws InterruptedException {
		try {
			logger.info("User login is in progress...");
			lp.succesLoginControl(user, password);
			logger.info("Data Analysis test is starting...");
			dap.openDataAnalysis().selectArea(area).selectCity(city).addLocation().addLocationControl(city).nextClick()
					.selectRefenceData(referenceData).selectQueryColumn(queryData).selectCriteriaColumn(criteriaData)
					.selectValue(criteriaData, value).clickAddQuery(queryType).clickQuery().uncheckedStandartLayers()
					.clickMarker().checkPopupData(criteriaData, queryData, value);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			Assert.fail(e.getMessage());
		}
	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {
		getDriver().close();
		logger.info("Test Finished...");
	}
}
