package com.p1m1.testautomation.locationplus.tuborg.test;

import static com.p1m1.testautomation.locationplus.util.ExcelUtils.getTableArray;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.p1m1.testautomation.locationplus.base.BaseTest;
import com.p1m1.testautomation.locationplus.tuborg.page.ExecutiveReportsPage;
import com.p1m1.testautomation.locationplus.tuborg.page.LoginPage;

public class ExecutiveReportsPageTest extends BaseTest {
	protected final Logger logger = Logger.getLogger(ExecutiveReportsPageTest.class);
	ExecutiveReportsPage erp;
	LoginPage lp;

	@DataProvider(name = "executiveReportsData")
	public Object[][] createLocations() {
		return getTableArray("properties/locationPlusExcel/"+getExcelName(), "executiveReports", "executiveReportsData");
	}

	@BeforeMethod
	public void before() throws InterruptedException {
		logger.info("Driver open...");
		lp = new LoginPage(getDriver());
		erp = new ExecutiveReportsPage(getDriver());
		logger.info("Test started");
	}

	@Test(description = "Executive Reports Test", dataProvider = "executiveReportsData")
	public void executiveReportsTest(String user, String pw, String comparasion, String value)
			throws InterruptedException {
		try {
			lp.succesLoginControl(user, pw);
			erp.openExecutiveReports().checkExecutiveUrl().selectAllArea().selectAllDealer().selectAllType()
					.selectAllSegment().selectComparasion(comparasion).fillValueField(value).clickGetResult();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			Assert.fail(e.getMessage());
		}
	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {
		getDriver().close();
		logger.info("Test Finished...");
	}
}
