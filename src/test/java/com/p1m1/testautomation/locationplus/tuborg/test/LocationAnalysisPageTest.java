package com.p1m1.testautomation.locationplus.tuborg.test;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.p1m1.testautomation.locationplus.base.BaseTest;
import com.p1m1.testautomation.locationplus.tuborg.page.LocationAnalysisPage;
import com.p1m1.testautomation.locationplus.tuborg.page.LoginPage;

import static com.p1m1.testautomation.locationplus.util.ExcelUtils.*;

public class LocationAnalysisPageTest extends BaseTest {
	protected final Logger logger = Logger.getLogger(LocationAnalysisPageTest.class);
	LocationAnalysisPage lap;
	LoginPage lp;
//	String city = "İZMİR";

	@DataProvider(name = "locations")
	public Object[][] createLocations() {
		return getTableArray("properties/locationPlusExcel/"+getExcelName(), "locationAnalysis", "locationAnalysisData");
	}

	@BeforeMethod
	public void before() throws InterruptedException {
		logger.info("Driver open...");
		lp = new LoginPage(getDriver());
		lap = new LocationAnalysisPage(getDriver());
		logger.info("Test started");
	}

	@Test(description = "Location Analysis", dataProvider = "locations")
	public void locationAnalysisControlTest(String user, String password, String area, String location,
			String customerName) throws InterruptedException {
		try {
			logger.info("User login being done...");
			lp.succesLoginControl(user, password);
			logger.info("DATAS : *****" + user + "*****" + password + " ***** " + area + "*****" + location + "****"
					+ customerName);
			logger.info("Location Analysis Test Starting...");
			lap.openLocationAnalysis().selectArea(area).selectLocation(location).addLocation()
					.addLocationControl(location).nextClick().searchCustomerName(customerName);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			Assert.fail(e.getMessage());
		}
	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {
		getDriver().close();
		logger.info("Test Finished...");
	}
}
