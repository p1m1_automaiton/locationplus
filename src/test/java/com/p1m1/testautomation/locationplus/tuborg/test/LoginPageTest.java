package com.p1m1.testautomation.locationplus.tuborg.test;

import static com.p1m1.testautomation.locationplus.util.ExcelUtils.getTableArray;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.p1m1.testautomation.locationplus.base.BaseTest;
import com.p1m1.testautomation.locationplus.tuborg.page.LoginPage;

public class LoginPageTest extends BaseTest {
	protected static final Logger logger = Logger.getLogger(LoginPageTest.class);
	LoginPage lp;
	
	@DataProvider(name = "loginUsers")
	public Object[][] createLocations() {
		return getTableArray("properties/locationPlusExcel/"+getExcelName(), "login", "loginData");
	}
	
	@BeforeMethod
	public void before() throws InterruptedException {
		logger.info("Driver open...");
		lp = new LoginPage(getDriver());
		logger.info("Test started");
	}

	/**
	 * Success login and control step
	 * 
	 * @throws InterruptedException
	 */
	@Test(description = "Succes Login Control", dataProvider = "loginUsers")
	public void succesLoginControlTest(String user, String password) throws InterruptedException {
		try {
			lp.succesLoginControl(user, password);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			Assert.fail(e.getMessage());
		}
	}

	@Test(description = "Succes Login Control")
	public void failLoginControlTest() throws InterruptedException {
		try {
			lp.failLoginControl();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			Assert.fail(e.getMessage());
		}
	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {
		getDriver().close();
		logger.info("Test Finished...");
	}
}
