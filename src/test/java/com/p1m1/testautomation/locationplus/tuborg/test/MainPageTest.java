package com.p1m1.testautomation.locationplus.tuborg.test;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.p1m1.testautomation.locationplus.base.BaseTest;
import com.p1m1.testautomation.locationplus.tuborg.page.MainPage;

public class MainPageTest extends BaseTest {
	protected static final Logger logger = Logger.getLogger(MainPageTest.class);

	MainPage mp;

	@BeforeMethod
	public void before() throws InterruptedException {
		logger.info("Driver open...");
		mp = new MainPage(getDriver());
		logger.info("Test started");
	}

	/**
	 * 
	 * @throws InterruptedException
	 */
	@Test(description = "Control Test")
	public void checkPageUrlTest() throws InterruptedException {
		try {
			mp.checkPageUrl();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			Assert.fail(e.getMessage());
		}
	}

	@Test(description = "Navigate Test")
	public void navigateTest() throws InterruptedException {
		try {
			mp.navigate();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			Assert.fail(e.getMessage());
		}
	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {
		getDriver().close();
		logger.info("Test Finished...");
	}
}
