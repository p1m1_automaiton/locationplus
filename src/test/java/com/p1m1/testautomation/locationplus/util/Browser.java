package com.p1m1.testautomation.locationplus.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.Platform;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

import com.p1m1.testautomation.locationplus.base.BaseTest;

public class Browser {
	protected final Logger logger = Logger.getLogger(Browser.class);
	private DesiredCapabilities capabilities;

	public void setBrowser(String browserName, String browserVersion, String url, int implicitlyWait)
			throws MalformedURLException {
		URL hub = new URL("http://82.222.181.130:4444/wd/hub");
		if (browserName != null) {
			if (("remoteFIREFOX").contains(browserName)) {
				FirefoxProfile profile = new FirefoxProfile();
				capabilities = DesiredCapabilities.firefox();
				profile.setPreference("browser.startup.homepage", "");
				profile.setPreference("intl.accept_languages", "no,en-us,en");
				capabilities.setBrowserName("firefox");
				capabilities.setVersion(browserVersion);
				capabilities.setPlatform(Platform.WINDOWS);
				BaseTest.setDriver(new RemoteWebDriver(hub, capabilities));
			}
			if (("remoteCHROME").equals(browserName)) {
				ChromeOptions options = new ChromeOptions();
				capabilities = DesiredCapabilities.chrome();
				options.addArguments("test-type");
				options.addArguments("disable-popup-blocking");
				options.addArguments("ignore-certificate-errors");
				options.addArguments("disable-translate");
				options.addArguments("start-maximized");
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				capabilities.setBrowserName("chrome");
				capabilities.setVersion(browserVersion);
				capabilities.setPlatform(Platform.WINDOWS);
				BaseTest.setDriver(new RemoteWebDriver(hub, capabilities));
			}
			if (("remoteIE").equals(browserName)) {
				capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setBrowserName("internet explorer");
				capabilities.setVersion(browserVersion);
				capabilities.setPlatform(Platform.WINDOWS);
				capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
						true);
				 capabilities.setCapability("ie.ensureCleanSession", true);
				capabilities.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING,false);
				capabilities.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, false);
				capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, false);
				capabilities.setJavascriptEnabled(true); 
				capabilities.setCapability("requireWindowFocus", true);
				BaseTest.setDriver(new RemoteWebDriver(hub, capabilities));
			}
			if (("FIREFOX").equals(browserName)) {
				FirefoxProfile profile = new FirefoxProfile();
				profile.setPreference("browser.startup.homepage", "");
				profile.setPreference("intl.accept_languages", "no,en-us,en");
				capabilities = DesiredCapabilities.firefox();
				capabilities.setPlatform(Platform.getCurrent());
				System.setProperty("webdriver.gecko.driver", "properties/driver/geckodriver.exe");
				BaseTest.setDriver(new FirefoxDriver(profile));
			}
			if (("SAFARI").equals(browserName)) {
				logger.info("Safari browser set:" + browserVersion);
				SafariOptions options = new SafariOptions();
				options.setUseCleanSession(true);
				capabilities = DesiredCapabilities.safari();
				capabilities.setBrowserName(browserName);
				capabilities.setVersion(browserVersion);
				capabilities.setPlatform(Platform.getCurrent());
				capabilities.setCapability(SafariOptions.CAPABILITY, options);
				System.setProperty("webdriver.safari.noinstall", "true");
				BaseTest.setDriver(new SafariDriver(capabilities));
			}
			if ("DEFAULT".equalsIgnoreCase(browserName)) {
				logger.info("Default browser set: Chrome");
				ChromeOptions options = new ChromeOptions();
				capabilities = DesiredCapabilities.chrome();
				options.addArguments("test-type");
				options.addArguments("disable-popup-blocking");
				options.addArguments("ignore-certificate-errors");
				options.addArguments("disable-translate");
				options.addArguments("start-maximized");
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				capabilities.setBrowserName("CHROME");
				capabilities.setVersion(browserVersion);
				capabilities.setPlatform(Platform.getCurrent());
				selectPath(capabilities.getPlatform());
				BaseTest.setDriver(new ChromeDriver(capabilities));
			}
		} else {
			logger.info("Default browser set: Chrome");
			ChromeOptions options = new ChromeOptions();
			capabilities = DesiredCapabilities.chrome();
			options.addArguments("test-type");
			options.addArguments("disable-popup-blocking");
			options.addArguments("ignore-certificate-errors");
			options.addArguments("disable-translate");
			options.addArguments("start-maximized");
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			capabilities.setBrowserName(browserName);
			capabilities.setVersion(browserVersion);
			capabilities.setPlatform(Platform.getCurrent());
			selectPath(capabilities.getPlatform());
			BaseTest.setDriver(new ChromeDriver(capabilities));
		}
		BaseTest.getDriver().manage().timeouts().implicitlyWait(implicitlyWait, TimeUnit.SECONDS);
		BaseTest.getDriver().manage().window().maximize();
		logger.info("Installation Complete");
		logger.info("********* BROWSER:" + capabilities.getBrowserName() + ", " + "VERSION:" + capabilities.getVersion()
				+ ", " + "PLATFORM:" + capabilities.getPlatform());
		BaseTest.getDriver().navigate().to(url);
	}

	protected void selectPath(Platform platform) {
		String browser;
		if ("CHROME".equals(capabilities.getBrowserName())) {
			browser = "webdriver.chrome.driver";
			switch (platform) {
			case MAC:
				System.setProperty(browser, "properties/driver/chromedrivermac");
				break;
			case WIN10:
			case WIN8:
			case WIN8_1:
			case WINDOWS:
				System.setProperty(browser, "properties/driver/chromedriverwin.exe");
				break;
			case LINUX:
				System.setProperty(browser, "properties/driver/chromedriverlinux64.exe");
				break;
			default:
				logger.info("PLATFORM DOES NOT EXISTS");
				break;
			}
		}
	}
}
