package com.p1m1.testautomation.locationplus.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.p1m1.testautomation.locationplus.base.BaseTest;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

public class TestListener extends BaseTest implements ITestListener {
	protected static final Logger logger = Logger.getLogger(TestListener.class);

	String timestampdate = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
	String timestampminute = new SimpleDateFormat("yyyyMMdd-HHmmss").format(Calendar.getInstance().getTime());
	String mainDirectory = "output/exec_results/";

	private String className;
	String browserScreenShot = "/screenShots_" + config.getBrowserName();

	@Override
	public void onStart(ITestContext testResult) {
		PropertyConfigurator.configure("properties/log4j.properties");
		logger.info("Start");
	}

	@Override
	public void onFinish(ITestContext testResult) {
		extentReports.flush();
		logger.info("Finished");
	}

	@Override
	public void onTestStart(ITestResult testResult) {
		// + getClassName() + "/" + timestampdate + "/reports/"+ getClassName()
		// +
		logger.info("Test Start :" + testResult.getName());
		logger.debug(testResult.getTestClass().getName());
		setClassName(testResult.getTestClass().getName());
		setMethodName(testResult.getName());
		if (extentReports == null) {
			extentReports = new ExtentReports(mainDirectory + "TestReports.html", false);
			extentReports.loadConfig(new File("properties/extentConfig.xml"));
			extentReports.addSystemInfo("Location+ - Test", "Selenium Test Automation");
			extentReports.assignProject("TUBORG LOCATION+");
		}
		extentTest = extentReports.startTest(testResult.getName());
		setTime();
		String imageLocation = mainDirectory + getClassName() + "/" + timestampdate + browserScreenShot + "/"
				+ getMethodName() + "-" + timestampminute + ".png";
		createFile();
		try {
			screenShot(imageLocation);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		imageLocation = getClassName() + "/" + timestampdate + browserScreenShot + "/" + getMethodName() + "-"
				+ timestampminute + ".png";
		extentTest.log(LogStatus.INFO, "Test Started :" + testResult.getName());
		extentTest.log(LogStatus.PASS, "START", extentTest.addScreenCapture(imageLocation));
	}

	@Override
	public void onTestSuccess(ITestResult testResult) {
		logger.debug(testResult.getTestClass().getName());
		setClassName(testResult.getTestClass().getName());
		setMethodName(testResult.getName());
		logger.info("TEST NAME :" + getMethodName());
		setTime();
		String imageLocation = mainDirectory + getClassName() + "/" + timestampdate + browserScreenShot + "/"
				+ getMethodName() + "-" + timestampminute + ".png";
		if (testResult.getStatus() == ITestResult.SUCCESS) {
			createFile();
			try {
				screenShot(imageLocation);
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
			imageLocation = getClassName() + "/" + timestampdate + browserScreenShot + "/" + getMethodName() + "-"
					+ timestampminute + ".png";
			extentTest.log(LogStatus.PASS, "SUCCES", extentTest.addScreenCapture(imageLocation));
			extentTest.log(LogStatus.PASS,
					"FINISH" + " Time :" + (testResult.getEndMillis() - testResult.getStartMillis()) + " ms");
			extentReports.endTest(extentTest);
		}
	}

	@Override
	public void onTestFailure(ITestResult testResult) {
		logger.debug(testResult.getTestClass().getName());
		setClassName(testResult.getTestClass().getName());
		logger.info("Test Failure-" + testResult.getName());
		setMethodName(testResult.getName());
		logger.info("TEST NAME :" + getMethodName());
		setTime();
		String imageLocation = mainDirectory + getClassName() + "/" + timestampdate + browserScreenShot + "/"
				+ getMethodName() + "-" + timestampminute + ".png";
		if (testResult.getStatus() == ITestResult.FAILURE) {
			createFile();
			try {
				screenShot(imageLocation);
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
			imageLocation = getClassName() + "/" + timestampdate + browserScreenShot + "/" + getMethodName() + "-"
					+ timestampminute + ".png";
			extentTest.log(LogStatus.ERROR, "ERROR :" + testResult.getThrowable(),
					extentTest.addScreenCapture(imageLocation));
			extentTest.log(LogStatus.ERROR,
					"FINISH" + " Time :" + (testResult.getEndMillis() - testResult.getStartMillis()) + " ms");
			extentReports.endTest(extentTest);
		}
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult testResult) {
		logger.fatal("onTestFailedButWithinSuccessPercentage");
	}

	@Override
	public void onTestSkipped(ITestResult testResult) {
		logger.info("TestSkipped");
	}

	/**
	 * @param imageLocation
	 * @throws IOException
	 */
	protected void screenShot(String imageLocation) throws IOException {
		File scrFile = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(imageLocation));
	}

	/**
	 * 
	 * Set Time
	 */
	protected void setTime() {
		timestampminute = new SimpleDateFormat("yyyyMMdd-HHmmss").format(Calendar.getInstance().getTime());
	}

	protected void createFile() {
		File file = new File(mainDirectory + getClassName() + "/" + timestampdate + browserScreenShot);
		if (!file.exists()) {
			if (file.mkdir()) {
				logger.info("Directory is created!");
			} else {
				logger.info("Failed to create directory!");
			}
		}
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		String[] classNameSet = className.split("\\.");
		String reClassName = classNameSet[classNameSet.length - 1];
		this.className = reClassName;
	}
}
